const MY_BASE_ROUTE = "https://api.jsonbin.io/v3";
export const MY_SECRET_KEY = "$2b$10$i2mExNKw/OV8VAMBv5xmaOg9gNUp81INGLHqmuflkHi2HN1788aa6";
export const MY_COLLECTIONS_ID = "605cd2f11b8d68569b0fa56c";

export function post(route : string, my_headers : JSON, my_initial_body? : JSON) {
  if (my_initial_body)
    return cy.request({
      method: 'POST',
      url: `${MY_BASE_ROUTE}${route}`,
      headers: my_headers,
      body: my_initial_body,
      failOnStatusCode: false
    });
  else
    return cy.request({
      method: 'POST',
      url: `${MY_BASE_ROUTE}${route}`,
      headers: my_headers,
      failOnStatusCode: false
    });
}

export function get(route : string, my_headers : JSON) {
  return cy.request({
    method: 'GET',
    url: `${MY_BASE_ROUTE}${route}`,
    headers: my_headers,
    failOnStatusCode: false
  });
}

export function put(route : string, my_headers : JSON, my_initial_body? : JSON) {
  return cy.request({
    method: 'PUT',
    url: `${MY_BASE_ROUTE}${route}`,
    headers: my_headers,
    body: my_initial_body,
    failOnStatusCode: false
  });
}

export function clean(route : string, my_headers : JSON) {
  return cy.request({
    method: 'DELETE',
    url: `${MY_BASE_ROUTE}${route}`,
    headers: my_headers,
    failOnStatusCode: false
  });
}