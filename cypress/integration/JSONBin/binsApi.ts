import * as big_json from "../../fixtures/large_file.json";
import { get, post, put, clean, MY_SECRET_KEY } from "../../support/helpers/shared";

describe("Bin API", () => {
  var my_bin_name = "Create_Bin_Mark";
  const bin_create_header = {
    "Content-Type": "application/json", 
    "X-Master-Key": MY_SECRET_KEY, 
    "X-Bin-Name": my_bin_name
  }
  const bin_read_header = {
    "X-Master-Key": MY_SECRET_KEY 
  }

  const bin_delete_header = {
    "X-Master-Key": MY_SECRET_KEY 
  }

  const bin_put_header = {
    "Content-Type": "application/json", 
    "X-Master-Key": MY_SECRET_KEY, 
  }
  var my_initial_body = {
    'sample':'Hello World'
  }
  let my_bin_id : Number;

  it("Happy Path: CRUD on Bin", () => {
    var my_new_body = {
      'sample': "Goodbye World"
    }
    
    post("/b", bin_create_header, my_initial_body).then((resp) => {
      expect(resp.status).to.eq(200);
      expect(resp.body.record).to.have.property("sample", "Hello World");
      expect(resp.body.metadata).to.have.property("id");
      my_bin_id = resp.body.metadata.id;
      expect(resp.body.metadata).to.have.property("name", my_bin_name);
      expect(resp.body.metadata).to.have.property("private", true);

      get(`/b/${my_bin_id}`, bin_read_header).then((resp) => {
        expect(resp.status).to.eq(200);
        expect(resp.body.record).to.have.property("sample", "Hello World");
      });

      put(`/b/${my_bin_id}`, bin_put_header, my_new_body).then((resp) => {
        expect(resp.status).to.eq(200);
        expect(resp.body.record).to.not.have.property("sample", "Hello World");
        expect(resp.body.record).to.have.property("sample", "Goodbye World");
      });

      clean(`/b/${my_bin_id}`, bin_delete_header, my_new_body).then((resp) => {
        expect(resp.status).to.eq(200);
      });
      get(`/b/${my_bin_id}`, bin_read_header).then((resp) => {
        expect(resp.status).to.eq(404);
      });
    });
  });

  it("Error 400: Create Bin, on Content-Type incorrect", () => {
    var broken_header = {
      "Content-Type": "text/css",
      "X-Master-Key": MY_SECRET_KEY, 
      "X-Bin-Name": "my_bin_name"
    }

    post("/b", broken_header, my_initial_body).then((resp) => {
      expect(resp.status).to.eq(400);
      expect(resp.statusText).to.eq("Bad Request")
    });
  });

  it("Error 400: Create Bin, on X-Bin-Name cannot be over 128 characters", () => {
    var broken_header = {
      "Content-Type": "application/json", 
      "X-Master-Key": MY_SECRET_KEY, 
      "X-Bin-Name": "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
    }

    post("/b", broken_header, my_initial_body).then((resp) => {
      expect(resp.status).to.eq(400);
      expect(resp.statusText).to.eq("Bad Request")
    });
  });

  it("Error 400: Create Bin, on X-Bin-Name cannot be blank", () => {
    var broken_header = {
      "Content-Type": "application/json", 
      "X-Master-Key": MY_SECRET_KEY, 
      "X-Bin-Name": ""
    }

    post("/b", broken_header, my_initial_body).then((resp) => {
      expect(resp.status).to.eq(400);
      expect(resp.statusText).to.eq("Bad Request")
    });
  });

  it("Error 400: Create Bin, on Invalid X-Collection-Id provided", () => {
    var broken_header = {
      "Content-Type": "application/json", 
      "X-Master-Key": MY_SECRET_KEY, 
      "X-Bin-Name": "test123",
      "X-Colection-Id": true
    }

    post("/b", broken_header, my_initial_body).then((resp) => {
      expect(resp.status).to.eq(400);
      expect(resp.statusText).to.eq("Bad Request")
    });
  });

  it("Error 401: Create Bin, on Unauthorized X-Master-Key", () => {
    var broken_header = {
      "Content-Type": "application/json", 
      "X-Master-Key": "0", 
      "X-Bin-Name": "test123",
      "X-Colection-Id": true
    }

    post("/b", broken_header, my_initial_body).then((resp) => {
      expect(resp.status).to.eq(401);
      expect(resp.statusText).to.eq("Unauthorized")
    });
  });

  it("Error 403: Create Bin, Forbidden (record too big for free user)", () => {
    var broken_header = {
      "Content-Type": "application/json", 
      "X-Master-Key": "0", 
      "X-Bin-Name": "test123",
      "X-Colection-Id": true
    }

    post("/b", broken_header, big_json).then((resp) => {
      console.log(resp);
      expect(resp.status).to.eq(401);
      expect(resp.statusText).to.eq("Unauthorized")
    });
  });


});