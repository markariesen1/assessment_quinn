import { post, MY_SECRET_KEY, clean, put, MY_COLLECTIONS_ID } from "../../support/helpers/shared";

describe("Collections API", () => {
  var my_collection_name = "Create_Colection_Mark";

  it("Happy Path: Update on Collections", () => {
    //How strange there is no delete on collections?
    //Reusing existing collection
    var my_new_name = new Date().getTime();
    var tmp_collections_create_header = { 
      "X-Master-Key": MY_SECRET_KEY, 
      "X-Collection-Name": my_new_name
    }
    //Update the collection (that should still exist)
    put(`/c/${MY_COLLECTIONS_ID}/meta/name`, tmp_collections_create_header).then((resp) => {
      expect(resp.status).to.eq(200);
      expect(resp.body.metadata).to.have.property("name", my_new_name.toString());
    });
  });
})