module.exports = {
  resolve: {
    extensions: [".ts", ".js", ".stj"]
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: "ts-loader"
          }
        ]
      },
      {
        test: /\.stj$/i,
        use: "raw-loader"
      }
    ]
  }
};
